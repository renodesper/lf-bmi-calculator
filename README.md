# BMI Calculator

This project serves as a learning process to learn Flutter. It was created based on Udemy course.

Reference: https://www.udemy.com/course/flutter-bootcamp-with-dart

## What you will learn

- How to use Flutter themes to create coherent branding.
- How to create multi-page apps using Flutter Routes and Navigator.
- How to extract and refactor Flutter Widgets with a click of the button.
- How to pass functions as parameters and fields.
- How to use the GestureDetector Widget to detect more than just a tap.
- How to use custom colour palettes by using hex codes.
- How to customise Flutter Widgets to achieve a specific design style.
- Understand Dart Enums and the Ternary Operator.
- Learn about composition vs. inheritance and the Flutter way of creating custom UI.
- Understand the difference between const and final in Dart and when to use each.

## Screenshot

![BMI Calculator 1](docs/screenshot1.jpg)

![BMI Calculator 2](docs/screenshot2.jpg)
